# Copyright 2010 Pierre Lejeune <superheron@gmail.com>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ]
require freedesktop-desktop gtk-icon-cache gsettings
require vala [ vala_dep=true ]

SUMMARY="Open source photo manager for GNOME"
DESCRIPTION="
Shotwell is a photo organizer for the GNOME desktop. It lets you import photos from disk or camera,
organize them in various ways, view them in full-window or fullscreen mode, and export them to share
with others.
"
HOMEPAGE="https://wiki.gnome.org/Apps/Shotwell"

LICENCES="LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    build:
        gnome-desktop/yelp-tools
        sys-devel/gettext[>=0.19.7]
        virtual/pkg-config[>=0.22]
    build+run:
        base/libgee:0.8[>=0.8.5]
        core/json-glib
        dev-db/sqlite:3[>=3.5.9]
        dev-libs/gexiv2[>=0.10.4]
        dev-libs/glib:2[>=2.40.0]
        dev-libs/libxml2:2.0[>=2.6.32]
        gnome-desktop/gcr
        gnome-desktop/libgdata
        gnome-desktop/libsoup:2.4[>=2.26.0]
        media-libs/gstreamer:1.0[>=1.0.0]
        media-libs/libexif[>=0.6.16]
        media-libs/libgphoto2[>=2.5.0]
        media-libs/libraw[>=0.13.2]
        media-plugins/gst-plugins-base:1.0[>=1.0.0]
        net-libs/rest[>=0.7]
        net-libs/webkit:4.0
        sys-apps/systemd
        x11-libs/gdk-pixbuf:2.0
        x11-libs/gtk+:3[>=3.14.0]
    run:
        gnome-desktop/librsvg:2
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --prefix=/usr
    --exec_prefix=/usr/$(exhost --target)

    --enable-publishers=all
    --with-authenticator=shotwell

    --disable-debug
    --disable-schemas-compile
    --disable-unity-support
)

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
    gsettings_pkg_postinst
}

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
    gsettings_pkg_postrm
}

